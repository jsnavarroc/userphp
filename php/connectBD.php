<?php 
 
/**
* Class connectBD
*
* @author Johan Navarro
* @date 23/05/2017
* 
* pasos conexión php con BD
* 1. conexión con la base de datos
* 2. hacer la consulta SQL
* 3. cierre de la conexión
*
*/
   class connectBD
   {
            /**
                * CUMPLE CON EL PASO 1 el metodo de construcción donde se establece
                * la conección con la base de datos y el servidor mysql.
            */
            public function connect(){
                global $connectionON;
                $server = "localhost";
                $root = "root";
                $password = "";
                $BD = "prueba";
                $connectionON = mysqli_connect($server, $root,  
                $password, $BD ) or die ("Failed to connect to database");
                if($connectionON){
                    // echo "Conección Existosa";
                }
            } 
            /**
                * CUMPLE CON EL PASO 2
                * @param string $sql
                * @return array resultado de la consulta sql, retorna una array con los datos de la consulta.
            */
            public function executeQuery($sql){
                global $connectionON;
                return mysqli_query($connectionON, $sql);  
            }
            
            /**
                * CUMPLE CON EL PASO 5 cierre de la conexión
            */
            public function closeConnect(){
                global $connectionON;
                $connectionOff = mysqli_close($connectionON);
              
            }
       
   }  
?>